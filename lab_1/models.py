from django.db import models
from django.utils import timezone

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.IntegerField()
    BOD = models.DateField(max_length=30)
    # TODO Implement missing attributes in Friend model

class Post(models.Model):
	author = models.ForeignKey(Friend, on_delete = models.CASCADE)
	content = models.CharField(max_length=125)
	published_date = models.DateTimeField(default=timezone.now)
