from typing import Text
from django import forms
from django.forms.widgets import TextInput, Textarea
from lab_2.models import Note

class NoteForm(forms.ModelForm):
  """Form definition for Note."""

  class Meta:
    """Meta definition for Noteform."""

    model = Note
    fields = "__all__"
    
    widgets = {
      'to' : forms.TextInput(
        attrs= {
          'class' : 'form-control bg-dark text-white border border-secondary',
          'placeholder' : 'To'
        }
      ),
      'from' : forms.TextInput(
        attrs= {
          'class' : 'form-control bg-dark text-white border border-secondary',
          'placeholder' : 'From'
        }
      ),
      'title' : forms.TextInput(
        attrs= {
          'class' : 'form-control bg-dark text-white border border-secondary',
          'placeholder' : 'Title'
        }
      ),
      'message' : forms.Textarea(
        attrs= {
          'class' : 'form-control bg-dark text-white border border-secondary',
          'placeholder' : 'Write your message here'
        }
      ),
    }