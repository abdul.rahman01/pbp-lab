from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import NoteForm
from lab_2.models import Note

# Create your views here.

def index(request):
  notes = Note.objects.all().values()
  response = {
    "notes": notes,
  }
  return render(request, 'lab4_index.html', response)

def note_list(request):
  notes = Note.objects.all().values()
  response = {
    "notes": notes,
  }
  return render(request, 'lab4_note_list.html', response)

def add_note(request):
  note_form = NoteForm(request.POST or None, request.FILES or None)
  # create object of form
    
  # check if form data is valid
  if note_form.is_valid():
    # save the form data to model
    note_form.save()
  response = {
    "forms": note_form,
  }
  if request.method == "POST":
    return HttpResponseRedirect("/lab-4")
  return render(request, "lab4_form.html", response)