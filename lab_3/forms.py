from django import forms
from django.forms.widgets import TextInput, Widget
from django.utils.regex_helper import Choice
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
  """FORMNAME definition."""

  # TODO: Define form fields here
  class Meta:
    model = Friend
    fields = "__all__"
    # fields = [
    #   'name',
    #   'BOD',
    #   'npm'
    # ]
    labels={
      'name':'Nama',
      'BOD':'DOB',
      'npm' : 'NPM',
    }
    widgets = {
      'BOD':TextInput(
        attrs={
          'type':'date',
        }
      ),
      'name':TextInput(
        attrs={
          'placeholder':'Masukkan nama'
        }
      ),
      'npm':TextInput(
        attrs={
          'placeholder':'Masukkan npm'
        }
      ),
    }


class FriendForm2(forms.Form):
  years_choice = range(1990,2022)
  nama = forms.CharField(
      max_length=30, 
      label="",
      widget=TextInput(
        attrs={
          "placeholder":"Enter Your Friend's Name",
          "class": "form",
        }
      )
    )
  npm = forms.CharField(
      label="",
      widget=TextInput(
        attrs={
          "placeholder":"Enter Your Friend's NPM",
          "class": "form",
        }
      ),max_length=10
    )
  DOB = forms.DateField(
    label="",
    widget=TextInput(
        attrs={
          "placeholder":"Enter Your Friend's DOB, format : yyyy-mm-dd",
          "class": "form",
          "type" : "date",
        }
      )
  )
