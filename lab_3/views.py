from django.http import response, HttpResponseRedirect
from django.shortcuts import render
from .forms import FriendForm, FriendForm2
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required(login_url='/admin/login/')
def index(request):
  friends_form = Friend.objects.all().values()
  response = {
    "forms": friends_form,
  }
  return render(request,  "lab3_index.html", response)

@login_required(login_url='/admin/login/')
def add_friend(request):
  friends_form = FriendForm(request.POST or None, request.FILES or None)
  # create object of form
    
  # check if form data is valid
  if friends_form.is_valid():
    # save the form data to model
    friends_form.save()
  response = {
    "forms": friends_form,
  }
  if request.method == "POST":
    return HttpResponseRedirect("/lab-3")
  return render(request, "lab3_form.html", response)

@login_required(login_url='/admin/login/')
def add_friend2(request):
  friends_form = FriendForm2(request.POST or None)

  if request.method == "POST":
    print (request)
    Friend.objects.create(
      name = request.POST['nama'],
      npm = request.POST['npm'],
      BOD = request.POST['DOB'],
    )
    return HttpResponseRedirect("/lab-3")
  context = {
    "form" : friends_form,
  }

  return render(request, 'myform.html', context)