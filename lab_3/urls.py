from django.urls import path
from .views import add_friend, index, add_friend2

urlpatterns = [
    path('add/', add_friend, name='add_friend'),
    path('add-2/', add_friend2, name='add_friend2'),
    path('', index, name='index'),

]