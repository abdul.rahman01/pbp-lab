import 'package:flutter/material.dart';

import 'package:flutter/material.dart';

void main() => runApp(const MyLabApp());

class MyLabApp extends StatelessWidget {
  const MyLabApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'VaksinYuk Login Page';

    return MaterialApp(
      title: appTitle,
      theme: ThemeData.light(),
      home: Scaffold(
        backgroundColor: Colors.lightBlue[400],
        appBar: AppBar(
          title: const Text(appTitle),
        ),
        body: const MyCustomForm(),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();
  String? emailValue = "";
  String? passwordValue = "";
  String validEmail = "pbpe02@xyz.com";
  String validPassword = "pbpe02";
  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          alignment: Alignment.center,
          child: const Text(
            "VaksinYuk",
            style: TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.w900,
                color: Colors.white,
                shadows: [
                  Shadow(
                    offset: Offset(0, 3),
                    blurRadius: 1,
                  ),
                  // Shadow(offset: Offset(0, -2)),
                  // Shadow(offset: Offset(0, -3)),
                  // Shadow(offset: Offset(0, -4)),
                  // Shadow(offset: Offset(0, -5)),
                  // Shadow(offset: Offset(0, -6)),
                  // Shadow(offset: Offset(0, -7)),
                ]),
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal:20),
          height: 250,
          child: Form(
            key: _formKey,
            child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.lightBlue[100],
                  borderRadius: BorderRadius.circular(20)),
              child: Container(
                // margin: EdgeInsets.all(15),
                padding: EdgeInsets.only(
                  left : 15,
                  right: 15
                  ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Login", style: TextStyle(fontSize: 15),),
                    TextFormField(
                      decoration: const InputDecoration(
                          hintText: "Masukkan email anda"),
                      // The validator receives the text that the user has entered.
                      validator: (value) {
                        emailValue = value;
                        if (value == null || value.isEmpty) {
                          return 'Tolong masukkan email anda';
                        }
                        if (value != validEmail) {
                          return 'Email belum terdaftar';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                          hintText: "Masukkan password anda"),
                      // The validator receives the text that the user has entered.
                      obscureText: true,
                      validator: (value) {
                        passwordValue = value;
                        if (value == null || value.isEmpty) {
                          return 'Tolong masukkan password anda';
                        }
                        return null;
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          // Validate returns true if the form is valid, or false otherwise.
                          if (_formKey.currentState!.validate()) {
                            // If the form is valid, display a snackbar. In the real world,
                            // you'd often call a server or save the information in a database.
                            if (emailValue == validEmail &&
                                passwordValue == validPassword) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                    content: Text('Sedang memperoses...')),
                              );
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                    content: Text(
                                        'Maaf, email atau password salah')),
                              );
                            }
                          }
                        },
                        child: const Text('Submit'),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}


// import './dummy_data.dart';
// import './screens/tabs_screen.dart';
// import './screens/meal_detail_screen.dart';
// import './screens/category_meals_screen.dart';
// import './screens/filters_screen.dart';
// import './screens/categories_screen.dart';
// import './models/meal.dart';

// void main() => runApp(MyLabApp());

// class MyLabApp extends StatefulWidget {
//   @override
//   _MyLabAppState createState() => _MyLabAppState();
// }

// class _MyLabAppState extends State<MyLabApp> {
//   Map<String, bool> _filters = {
//     'gluten': false,
//     'lactose': false,
//     'vegan': false,
//     'vegetarian': false,
//   };
//   List<Meal> _availableMeals = DUMMY_MEALS;
//   List<Meal> _favoriteMeals = [];

//   void _setFilters(Map<String, bool> filterData) {
//     setState(() {
//        _filters = filterData;

//       _availableMeals = DUMMY_MEALS.where((meal) {
//         if (_filters['gluten']! && !meal.isGlutenFree) {
//           return false;
//         }
//         if (_filters['lactose']! && !meal.isLactoseFree) {
//           return false;
//         }
//         if (_filters['vegan']!&& !meal.isVegan) {
//           return false;
//         }
//         if (_filters['vegetarian']! && !meal.isVegetarian) {
//           return false;
//         }
//         return true;
//       }).toList();
//     });
//   }

//   void _toggleFavorite(String mealId) {
//     final existingIndex =
//         _favoriteMeals.indexWhere((meal) => meal.id == mealId);
//     if (existingIndex >= 0) {
//       setState(() {
//         _favoriteMeals.removeAt(existingIndex);
//       });
//     } else {
//       setState(() {
//         _favoriteMeals.add(
//           DUMMY_MEALS.firstWhere((meal) => meal.id == mealId),
//         );
//       });
//     }
//   }

//   bool _isMealFavorite(String id) {
//     return _favoriteMeals.any((meal) => meal.id == id);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Daftar Makanan',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//         accentColor: Colors.indigo,
//         canvasColor: Color.fromRGBO(255, 254, 229, 1),
//         fontFamily: 'Raleway',
//         textTheme: ThemeData.light().textTheme.copyWith(
//             bodyText1: TextStyle(
//               color: Color.fromRGBO(20, 51, 51, 1),
//             ),
//             bodyText2: TextStyle(
//               color: Color.fromRGBO(20, 51, 51, 1),
//             ),
//             headline6: TextStyle(
//               fontSize: 20,
//               fontFamily: 'RobotoCondensed',
//               fontWeight: FontWeight.bold,
//             )),
//       ),
//       // home: CategoriesScreen(),
//       initialRoute: '/', // default is '/'
//       routes: {
//         '/': (ctx) => TabsScreen(_favoriteMeals),
//         CategoryMealsScreen.routeName: (ctx) =>
//             CategoryMealsScreen(_availableMeals),
//         MealDetailScreen.routeName: (ctx) => MealDetailScreen(_toggleFavorite, _isMealFavorite),
//         FiltersScreen.routeName: (ctx) => FiltersScreen(_filters, _setFilters),
//       },
//       onGenerateRoute: (settings) {
//         print(settings.arguments);
//       },
//       onUnknownRoute: (settings) {
//         return MaterialPageRoute(
//           builder: (ctx) => CategoriesScreen(),
//         );
//       },
//     );
//   }
// }
