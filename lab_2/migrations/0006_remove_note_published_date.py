# Generated by Django 3.2.7 on 2021-10-15 12:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0005_note_published_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='note',
            name='published_date',
        ),
    ]
