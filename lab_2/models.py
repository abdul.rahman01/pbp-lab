from django.db import models
from django.utils import timezone

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=30,)
    from_var = models.CharField(max_length=30, name="from")
    title = models.CharField(max_length=30)
    message = models.TextField()
    # published_date = models.DateTimeField(default=timezone.now)
