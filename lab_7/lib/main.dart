import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  bool _isVisible = true;
  String? emailValue = "";
  String? passValue = "";
  Map<String, String> accounts = {
    "abc@xyz.com" : "abc",
    "def@xyz.com" : "def",
    "ghi@xyz.com" : "ghi",
    "jkl@xyz.com" : "jkl",
    "mno@xyz.com" : "mno",
    "pqr@xyz.com" : "pqr",
    "stu@xyz.com" : "stu",
    "vwx@xyz.com" : "vwx",
    "yz@xyz.com" : "yz",
  }; // email : password

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue[300],
      // appBar: AppBar(
      //   title: const Text("VaksinYuk | Login"),
      // ),
      body: Form(
        key: _formKey,
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("VaksinYuk",
                      style: TextStyle(
                        shadows: const [
                          Shadow(
                              color: Color.fromRGBO(204, 204, 204, 1.0),
                              offset: Offset(0, 1),
                              blurRadius: 0),
                          Shadow(
                              color: Color.fromRGBO(201, 201, 201, 1.0),
                              offset: Offset(0, 2),
                              blurRadius: 0),
                          Shadow(
                              color: Color.fromRGBO(187, 187, 187, 1.0),
                              offset: Offset(0, 3),
                              blurRadius: 0),
                          Shadow(
                              color: Color.fromRGBO(185, 185, 185, 1.0),
                              offset: Offset(0, 4),
                              blurRadius: 0),
                          Shadow(
                              color: Color.fromRGBO(170, 170, 170, 1.0),
                              offset: Offset(0, 5),
                              blurRadius: 0),
                          Shadow(
                              color: Color.fromRGBO(0, 0, 0, 0.1),
                              offset: Offset(0, 6),
                              blurRadius: 1),
                          Shadow(
                              color: Color.fromRGBO(0, 0, 0, 0.1),
                              offset: Offset(0, 0),
                              blurRadius: 5),
                          Shadow(
                              color: Color.fromRGBO(0, 0, 0, 0.3),
                              offset: Offset(0, 1),
                              blurRadius: 3),
                          Shadow(
                              color: Color.fromRGBO(0, 0, 0, 0.2),
                              offset: Offset(0, 3),
                              blurRadius: 5),
                          Shadow(
                              color: Color.fromRGBO(0, 0, 0, 0.25),
                              offset: Offset(0, 5),
                              blurRadius: 10),
                          // Shadow(color: Color.fromRGBO(0, 0, 0,0.2),  offset: Offset(0, 10), blurRadius : 10),
                          // Shadow(color: Color.fromRGBO( 0, 0, 0, 0.15), offset: Offset(0, 20), blurRadius : 20),
                          // Shadow(color: Color(0xCCCCCCCC), offset: Offset(0, 7), blurRadius : 0),
                          // 0 1px 0 #ccc,
                          // 0 2px 0 #c9c9c9,
                          // 0 3px 0 #bbb,
                          // 0 4px 0 #b9b9b9,
                          // 0 5px 0 #aaa,
                          // 0 6px 1px rgba(0,0,0,.1),
                          // 0 0 5px rgba(0,0,0,.1),
                          // 0 1px 3px rgba(0,0,0,.3),
                          // 0 3px 5px rgba(0,0,0,.2),
                          // 0 5px 10px rgba(0,0,0,.25),
                          // 0 10px 10px rgba(0,0,0,.2),
                          // 0 20px 20px rgba(0,0,0,.15);
                        ],
                        fontSize: 50,
                        color: Colors.lightBlue[100],
                        fontWeight: FontWeight.w900,
                        fontFamily: "Poppins",
                      )),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  decoration: BoxDecoration(
                    color: Colors.lightBlue[50],
                    borderRadius: BorderRadius.circular(20),
                  ),
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text("Login"),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: InputDecoration(
                            hintText: "Contoh : abc@xyz.com",
                            labelText: "Email",
                            icon: const Icon(Icons.mail),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                          ),
                          validator: (value) {
                            emailValue = value;
                            if (value == null || value.isEmpty) {
                              return 'Email tidak boleh kosong';
                            } else if (!accounts.containsKey(value)){
                              return 'Email belum terdaftar';
                            }
                            return null;
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          obscureText: _isVisible,
                          decoration: InputDecoration(
                            suffixIcon: GestureDetector(
                              onTap: () {
                                print("clicked");
                                setState(() {
                                  _isVisible = !_isVisible;
                                });
                              },
                              child: (_isVisible)
                                  ? Icon(Icons.visibility_off)
                                  : Icon(Icons.visibility),
                            ),
                            labelText: "Password",
                            icon: const Icon(Icons.security),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                          ),
                          validator: (value) {
                            passValue = value;
                            if (value == null || value.isEmpty) {
                              return 'Password tidak boleh kosong';
                            }
                            return null;
                          },
                        ),
                      ),
                      Row(
                        children: [
                          Checkbox(
                              value: nilaiCheckBox,
                              onChanged: (value) {
                                setState(() {
                                  if (value != null) {
                                    nilaiCheckBox = value;
                                  }
                                });
                              }),
                          Text("Simpan Email?"),
                        ],
                      ),
                      // CheckboxListTile(
                      //   title: const Text('Ingat saya'),
                      //   // subtitle: const Text('Dart, widget, http'),
                      //   value: nilaiCheckBox,
                      //   contentPadding: EdgeInsets.all(0),
                      //   activeColor: Colors.deepPurpleAccent,
                      //   onChanged: (value) {
                      //     setState(() {
                      //       if (value != null) {
                      //         nilaiCheckBox = value;
                      //       }
                      //     });
                      //   },
                      // ),
                      // SwitchListTile(
                      //   title: const Text('Backend Programming'),
                      //   subtitle: const Text('Dart, Nodejs, PHP, Java, dll'),
                      //   value: nilaiSwitch,
                      //   activeTrackColor: Colors.pink[100],
                      //   activeColor: Colors.red,
                      //   onChanged: (value) {
                      //     setState(() {
                      //       nilaiSwitch = value;
                      //     });
                      //   },
                      // ),
                      // Slider(
                      //   value: nilaiSlider,
                      //   min: 0,
                      //   max: 100,
                      //   onChanged: (value) {
                      //     setState(() {
                      //       nilaiSlider = value;
                      //     });
                      //   },
                      // ),
                      ElevatedButton(
                        child: const Text(
                          "Login",
                          style: TextStyle(color: Colors.white),
                        ),
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all<Color>(Colors.blue)),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            if (accounts[emailValue] != passValue) {
                              print("tidak bisa login");
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                    content: Text('Email atau password tidak sesuai')),
                              );
                            } else {
                            print(""" 
                            Email    : $emailValue
                            Password : $passValue
                            """);
                            ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                    content: Text('Sedang memperoses...')),
                              );
                          }

                            }
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text("Belum punya akun, silahkan ",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            )),
                        GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => Register()));
                            },
                            child: Text(
                              "register",
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                                color: Colors.blue.shade900,
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                      ]),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Register extends StatelessWidget {
  const Register({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Text("Login"),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
