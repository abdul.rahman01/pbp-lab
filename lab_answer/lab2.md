**Pertanyaan**

1. Apakah perbedaan antara JSON dan XML?
2. Apakah perbedaan antara HTML dan XML?
3.

**Jawaban**

1. Secara umum perbedaan antara `JSON` dan `XML` terletak pada sintaks penulisannya. JSON atau *JavaScript Object Notation* ditulis mirip dengan sintaks JavaScript saat kita membuat Object pada sintaks JavaScript. Sintkas ini dituliskan mirip dengan sintaks dictionary pada Python, yakni dimulai dengan dan diakhiri dengan kurung kurawal `"{}"` dan terdiri dari pasangan key dan value. *Key* dalam hal ini harus berupa `string`, sedangkan *value* bisa berupa `Boolean, Array, String, Object, Number, null` (BASONN). Jika, terdiri dari dua object, maka awalnya harus dimulai dengan kurung siku `"[]"`.

   Sedangkan `XML`, atau *eXtensible Markup Language* ditulis dengan menggunakan tag-tag. Penulisan tag-tag dalam XML mirip dengan dengnulisan pada HTML yakni dibuka dengan tag pembuka (misal `<tag>`) dan diakhir dengan tag penutup (misal `</tag>`). Akan tetapi, tag yang dimiliki oleh XML merupakan tag-tag yang custom sesuai dengan Object yang dimiliki.
2. Secara umum, perbedaan `HTML` dan `XML` terletak pada kegunaan dari kedua file ini. `HTML` atau *HyperText Markup Language* digunkan untuk menampilkan laman website. Jadi hampir semua yang kita lihat pada website itu merupakan file `HTML` yang kemuidan browser kita mengkonversinya menjadi tampilan yang lebih indah dan mudah untuk dipahami. Sedangkan `XML` biasanya digunakan untuk komunikasi atau penghubung antara API untuk agar website kita dapat mengakses data dari API tersebut. Selain itu, walupun `XML` dan `HTML` punya sintaks yang sama, bukan berarti web browser juga bisa mengkonversinya menjadi tampilan web yang indah, karena tag-tag pada `XML` merupakan tag=tag yang custom sesuai dengan object pada API yang diambil.

   Salah satu contohnya dalam tugas minggu ini adalah pada penggunaan object Note. Jadi nantinya object Note yang disimpan bisa kita transferkan datanya ke website kita, bahkan ke website orang lain pun bisa dengan melaui perantara file `XML` ini.

Sumber:

1. [REST API #3 APA ITU JSON?](https://www.youtube.com/watch?v=EluVFXu4GOUhttps:/)
2. [Pengantar API, JSON, dan XML](https://www.youtube.com/watch?v=Q-Z-bjDcxFghttps:/)
