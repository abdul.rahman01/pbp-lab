from django.shortcuts import render
from datetime import datetime, date

def index(request):
    response = {
        "judul" : "Praktikum PBP",
        "subjudul" : "Web-Based Platform",
    }
    return render(request, 'index.html', response)